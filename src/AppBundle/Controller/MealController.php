<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MealController extends Controller
{
    /**
     * @Route("/meal", name="meal")
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Meal');
		$meals = $repository->findAll();
        return $this->render('meal/index.html.twig', [
            'meals' => $meals,
        ]);
    }
	
    /**
     * Matches /meal/*
     *
     * @Route("/meal/{id}", name="meal_show")
     */
    public function showAction($id)
    {
		$meal = $this->getDoctrine()
			->getRepository('AppBundle:Meal')
			->find($id);
			
		if (!$meal) {
			throw $this->createNotFoundException(
				'No meal found for id '.$id
			);
		}
		
        return $this->render('meal/detail.html.twig', [
            'meal' => $meal,
        ]);
    }
}
