<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Order;

class OrderController extends Controller
{
    /**
     * @Route("/order", name="order")
     */
    public function indexAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$fields = array('o.id', 'o.publishedAt', 'm.title', 'm.restaurant');
		$filterbydate = $request->request->get('date');		
		
		$query = $em->createQueryBuilder();
		$query
			->select($fields)
			->from('AppBundle:Order', 'o')
			->innerJoin('o.meals', 'm');
		if(isset($filterbydate) && !empty($filterbydate) && $this->validateDate($filterbydate))
			$query->where("o.publishedAt = '$filterbydate'");		
		$orders = $query->getQuery()->getResult();
		
        return $this->render('order/index.html.twig', [
            'orders' => $orders,
        ]);
    }
	
    /**
     * @Route("/order/make", name="order_make")
     */
    public function makeAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Meal');
		$meals = $repository->findAll();
        return $this->render('order/make.html.twig', [
            'meals' => $meals,
        ]);
    }
	
	private function validateDate($date, $format = 'Y-m-d')
	{
		$d = \DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	
    /**
     * @Route("/order/save", name="order_save")
     */
    public function saveAction(Request $request)
    {
		if(!$this->validateDate($request->request->get('publishedAt'))) return $this->redirectToRoute('order_make'); 
		$order = new Order();
		$order->setPublishedAt(new \DateTime($request->request->get('publishedAt')));
		$meal = $this->getDoctrine()
			->getRepository('AppBundle:Meal')
			->find((int)$request->request->get('meal'));		
		$order->addMeal($meal);
		$meal->addOrder($order);
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
		$em->persist($meal);
        $em->flush();		
		return $this->redirectToRoute('order');
    }
}
