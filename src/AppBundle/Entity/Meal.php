<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="meals")
 */
class Meal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Order", inversedBy="meals", cascade={"persist"})
     * @ORM\JoinTable(name="meal_order",
     * joinColumns={@ORM\JoinColumn(name="meal_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")}
     * )
     */
    private $orders;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $title;
	
    /**
     * @ORM\Column(type="decimal")
     * @Assert\Currency()
     */
    private $price;
	
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $restaurant;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $publishedAt;

    public function __construct()
    {
        //$this->publishedAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
	
    public function getRestaurant()
    {
        return $this->restaurant;
    }
    public function setRestaurant($restaurant)
    {
        $this->restaurant = $restaurant;
    }

    public function getPublishedAt()
    {
        return $this->publishedAt;
    }
    public function setPublishedAt(\DateTime $publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    public function getOrder()
    {
        return $this->order;
    }
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }
	
    public function addOrders(Order $orders)
    {
        $this->orders[] = $orders;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\Order $order
     *
     * @return Meal
     */
    public function addOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\Order $order
     */
    public function removeOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }
}
