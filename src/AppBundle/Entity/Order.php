<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="orders")
 */
class Order
{
	//@ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
    /**
     * Use constants to define configuration options that rarely change instead
     * of specifying them in app/config/config.yml.
     * See http://symfony.com/doc/current/best_practices/configuration.html#constants-vs-configuration-options
     */
    const NUM_ITEMS = 10;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $publishedAt;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Meal",
     *      mappedBy="orders",
     *      cascade={"persist"}
     * )
     * @ORM\OrderBy({"publishedAt" = "DESC"})
     */
    private $meals;

    public function __construct()
    {
        //$this->publishedAt = new \DateTime();
        $this->meals = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTime $publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }
	
    public function addMeals(Meal $meals)
    {
        $this->meals[] = $meals;
    }

    public function getMeals()
    {
        return $this->meals;
    }

    public function addMeal(Meal $meal)
    {
        $this->meals->add($meal);
        $meal->setOrder($this);
    }

    public function removeMeal(Meal $meal)
    {
        $this->meals->removeElement($meal);
    }
}
